import os
import random
import time

import openai
from flask import Flask, render_template, request

import utils

app = Flask(__name__)
openai.api_key = os.getenv("OPENAI_API_KEY")

DEFAULT_INPUTS = [
    "An eye for an eye; a tooth for a tooth",
    "All happy families are alike; each unhappy family is unhappy in its own way.",
    "There's always money in the banana stand",
    "See no evil, hear no evil, say no evil",
]


# Used in debug mode to create a fake response
EMOJI = [
    "\N{RUNNER}",
    "\N{BRAIN}",
    "\N{SNAKE}",
]


@app.route("/")
def index():
    return render_template("home.html", default_input=random.choice(DEFAULT_INPUTS))


def generate_prompt(text):
    return [
        {
            "role": "system",
            "content": "Rewrite the given text, but _only use emoji_. Try to make your response funny and amusing.",
        },
        {"role": "user", "content": "I think you should leave"},
        # Fun fact: Python 3.10 is only on Unicode 13, which does not include "INDEX POINTING AT THE VIEWER",
        # so we have to specify it with the \Uxxxxxxxx syntax.
        {
            "role": "assistant",
            "content": "\N{EYE}\N{BRAIN}\U0001faf5\N{RUNNER}\N{DASH SYMBOL}",
        },
        {"role": "user", "content": text},
    ]


@app.post("/summarize")
def summarize():
    temperature = utils.clamp(float(request.form["temperature"]), low=0, high=1)
    if app.debug:
        time.sleep(1)  # Emulate the delay the OpenAI call takes
        return "".join(random.choices(EMOJI, k=random.randint(1, 5)))
    else:
        try:
            response = openai.ChatCompletion.create(
                model="gpt-3.5-turbo",
                messages=generate_prompt(request.form["text"]),
                temperature=temperature,
                max_tokens=60,  # approximately 30 emoji
                frequency_penalty=1,
                n=1,
            )
        except Exception as e:
            app.logger.error(e)
            # See the 'htmx:beforeSwap' listener in custom.js; htmx will show
            # this error message where the response usually appears
            return ("Sorry, something went wrong \N{GRIMACING FACE}", 500)
        return f"{response.choices[0].message.content}"
