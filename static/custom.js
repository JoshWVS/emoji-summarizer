// Taken from https://sentry.io/answers/how-do-i-copy-to-the-clipboard-in-javascript/
async function copyTextToClipboard(textToCopy) {
  try {
    if (navigator?.clipboard?.writeText) {
      await navigator.clipboard.writeText(textToCopy);
    }
  } catch (err) {
    console.error(err);
  }
}

function copySummary() {
  var input = document.getElementById("input-text");
  var summary = document.getElementById("result");
  copyTextToClipboard(input.value + " = " + summary.value);
  var tooltip = document.getElementById("copied-tooltip");
  tooltip.style.visibility = "visible";
  window.setTimeout(() => {  tooltip.style.visibility = "hidden"; }, 500);
}

// Adapted from https://dev.htmx.org/docs/#modifying_swapping_behavior_with_events
document.body.addEventListener('htmx:beforeSwap', function(evt) {
    // Even if we receive a 500, htmx should still proceed with the swap (so
    // that the user is informed of the error)
    if (evt.detail.xhr.status === 500) {
        // set isError to false to avoid error logging in console
        evt.detail.shouldSwap = true;
        evt.detail.isError = false;
    }
});
