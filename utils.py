def clamp(x, low=0, high=1):
    """Constrain the input value to [low, high]"""
    return min(max(x, low), high)
